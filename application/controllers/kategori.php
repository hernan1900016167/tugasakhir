<?php
class Kategori extends CI_Controller
{
    public function lokal()
    {
        $data['lokal'] = $this->model_kategori->data_lokal()->result();
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('lokal', $data);
        $this->load->view('templates/footer');
    }
    public function impor()
    {
        $data['impor'] = $this->model_kategori->data_impor()->result();
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('impor', $data);
        $this->load->view('templates/footer');
    }
}
