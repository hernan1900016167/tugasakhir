<?php
class Model_kategori extends CI_Model
{
    public function data_lokal()
    {
        return $this->db->get_where('tb_barang', array('kategori' => 'Lokal'));
    }
    public function data_impor()
    {
        return $this->db->get_where('tb_barang', array('kategori' => 'Impor'));
    }
}
